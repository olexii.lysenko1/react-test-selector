import React from 'react';
import { Routes, Route } from 'react-router-dom';

import StoreProvider from './store/storeProvider';

import Navbar from './components/Navbar';
import Home from './pages';
import Posters from './pages/product/posters.js';
import Flyers from './pages/product/flyers.js';
import BusinessCards from './pages/product/businessCards.js';
import Cart from './pages/cart';

import './App.css';

function App() {
    return (
        <StoreProvider>
            <Navbar />
            <Routes>
                <Route path='/' element={<Home />} />
                <Route path='/product/posters' element={<Posters />} />
                <Route path='/product/flyers' element={<Flyers />} />
                <Route path='/product/businesscards' element={<BusinessCards />} />
                <Route path='/cart' element={<Cart />} />
            </Routes>
        </StoreProvider>
    );
}

export default App;
