import * as types from '../types/summaryTypes.js';

export const setIsSummaryShown = (booleanValue) => ({
    type: types.SET_IS_SUMMARY_SHOWN,
    payload: booleanValue,
});
