import * as types from '../types/businessCardsTypes.js';

export const setSize = (size) => ({
    type: types.SET_SIZE,
    payload: size,
});
