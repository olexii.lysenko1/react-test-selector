import React, { useReducer } from 'react';
import initialState from './initialState';
import { combineReducers } from './reducers/combineReducers';

import businessCards from './reducers/businessCardsReducer';
import flyers from './reducers/flyersReducer';
import posters from './reducers/postersReducer';
import summary from './reducers/summaryReducer';

const rootReducer = combineReducers({ businessCards, flyers, posters, summary });
export const StoreContext = React.createContext();

const StoreProvider = ({ children }) => {
    const [state, dispatch] = useReducer(rootReducer, initialState);
    // Important(!): memoize array value. Else all context consumers update on *every* render
    const store = React.useMemo(() => [state, dispatch], [state]);
    return (
        <StoreContext.Provider value={store}> {children} </StoreContext.Provider>
    );
};

export default StoreProvider;
