export default {
    businessCards: {
        size: null,
    },
    posters: {
        size: null,
    },
    flyers: {
        size: null,
    },
    summary: {
        isSummaryShown: false,
    },
}
