import * as types from '../types/businessCardsTypes';
import initialState from '../initialState';

export default function businessCardsReducer(state = initialState.businessCards, action) {
    switch (action.type) {
        case types.SET_SIZE:
            return { ...state, size: action.payload }
        case types.RESET:
            return initialState
        default:
            return state
    }
}
