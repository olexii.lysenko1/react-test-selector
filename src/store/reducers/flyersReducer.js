import initialState from '../initialState';

export default function flyersReducer(state = initialState.flyers, action) {
    switch (action) {
        case 'setSize':
            return { ...state, size: action.payload }
        case 'reset':
            return initialState
        default:
            return state
    }
}
