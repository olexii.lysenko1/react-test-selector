import * as types from '../types/summaryTypes';
import initialState from '../initialState';

export default function summaryReducer(state = initialState.summary, action) {
    switch (action.type) {
        case types.SET_IS_SUMMARY_SHOWN:
            return { ...state, isSummaryShown: action.payload }
        default:
            return state
    }
}
