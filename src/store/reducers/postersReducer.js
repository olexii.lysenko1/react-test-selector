import initialState from '../initialState';

export default function postersReducer(state = initialState.posters, action) {
    switch (action) {
        case 'setSize':
            return { ...state, size: action.payload }
        case 'reset':
            return initialState
        default:
            return state
    }
}
