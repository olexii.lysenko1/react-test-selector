import React, { useContext } from 'react';
import * as businessCardsData from '../../fakeApiData/businessCardsData.json';
import { PROPERTIES } from '../../services/businessCardsService';

import ProductSizes from '../../components/ProductSizes';
import ProductCard from '../../components/ProductCard';
import ProductPage from '../../components/ProductPage';
import ProductSummary from '../../components/ProductSummary';

import { StoreContext } from '../../store/storeProvider'

const BusinessCards = () => {
    const { properties } = businessCardsData.default;
    const sizeData = properties.find((item) => item.slug === PROPERTIES.SIZE) || [];
    const [state, dispatch] = useContext(StoreContext);
    const { isSummaryShown } = state.summary;

    return (
        <>
            <ProductPage>
                <ProductCard />
                <ProductSizes data={sizeData} />
            </ProductPage >
            {isSummaryShown && <ProductSummary />}
        </>
    )
};

export default BusinessCards;
