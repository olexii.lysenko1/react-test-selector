import React from 'react';
import * as postersData from '../../fakeApiData/postersData.json'

const Posters = () => {
    return (
        <div
            style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                height: '90vh'
            }}
        >
            <h1>Posters</h1>
        </div>
    );
};

export default Posters;
