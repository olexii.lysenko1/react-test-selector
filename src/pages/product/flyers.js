import React from 'react';
import * as flyersData from '../../fakeApiData/flyersData.json'

const Flyers = () => {
    return (
        <div
            style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                height: '90vh'
            }}
        >
            <h1>Flyers</h1>
        </div>
    );
};

export default Flyers;
