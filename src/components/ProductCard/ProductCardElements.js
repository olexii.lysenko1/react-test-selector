import styled from 'styled-components';

export const ProductCardContainer = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    flex-wrap: wrap;
    max-width: 1200px;
    margin: 40px 0;
`;

export const ProductCardCard = styled.div`
    position: relative;
    min-width: 320px;
    height: 440px;
    box-shadow: inset 5px 5px 5px rgba(0,0,0,0.2),
                inset -5px -5px 15px rgba(255,255,255,0.1),
                5px 5px 15px rgba(0,0,0,0.3),
                -5px -5px 15px rgba(255,255,255,0.1);
    border-radius: 15px;

`;

export const ProductCardBox = styled.div`
    position: absolute;
    top: 20px;
    left: 20px;
    right: 20px;
    bottom: 20px;
    background: #2a2b2f;
    border: 2px solid #1e1f23;
    border-radius: 15px;
    box-shadow: 0 20px 50px rgba(0,0,0,0.5); 
    transition: 0.5s;
    display: flex;
    justify-content: center;
    align-items: center;

    &:hover {
        transform: translateY(-50px);
        box-shadow: 0 40px 70px rgba(0,0,0,0.5);
    }
`;

export const ProductCardContent = styled.div`
    padding: 20px;
    text-align: center;

    & h3 {
        font-size: 1.8em;
        color: rgba(255,255,255,0.5);
    }

    & p {
        margin-top: 10px;
        font-size: 16px;
        font-weight: 300;
        color: rgba(255,255,255,0.5);
        z-index: 1;
        transition: 0.5s;
    }

    & div {
        position: relative;
        display: block;
        margin-top: 60px;
        margin-left: 45px;
        width: 150px;
        height: 50px;
        line-height: 48px;
        background: #000;
        text-transform: uppercase;
        font-size: 20px;
        text-decoration: none;
        border-radius: 5px;

        span {
            position: absolute;
            display: block;
            width: 148px;
            top: 1px;
            right: 1px;
            bottom: 1px;
            text-align: center;
            background: #0c0c0c;
            color: rgba(255,255,255,0.2);
            transition: 0.5s;
            z-index: 1;
            border-radius: 5px;

            &:hover {
                color: rgba(255,255,255,1);
            }

            &::before {
                content: '';
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 50%;
                background: rgba(255,255,255,0.1);
            }
        }
    }

    & div::before {
        content: '';
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background: linear-gradient(45deg, #fb0094, #00f, #0f0, #ff0, #f00,  #fb0094, #00f, #0f0, #ff0, #f00);
         background-size: 400%;
        opacity: 0;
        transition: 0.5s;
        animation: animate 20s linear infinite;
        border-radius: 5px;
    }

    & div::after {
        content: '';
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background: linear-gradient(45deg, #fb0094, #00f, #0f0, #ff0, #f00,  #fb0094, #00f, #0f0, #ff0, #f00);
         background-size: 400%;
        opacity: 0;
        transition: 0.5s;
        filter: blur(20px);
        transition: 0.5s;
        animation: animate 20s linear infinite;
    }

    & div:hover::before,
    div:hover::after {
       opacity: 1;
    }

    @keyframes animate {
        0% {
            background-position: 0 0;
        }
        50% {
        background-position: 300% 0;
        }
        100% {
        background-position: 0 0;
        }
    }
`;
