import React, { useContext } from 'react';
import { StoreContext } from '../../store/storeProvider'
import {
    ProductCardContainer,
    ProductCardCard,
    ProductCardBox,
    ProductCardContent,
} from './ProductCardElements';
import { setIsSummaryShown } from '../../store/actions/summaryActions';

const ProductCard = () => {
    const [state, dispatch] = useContext(StoreContext);

    const handleOrderClick = () => {
        dispatch(setIsSummaryShown(true));
        console.log('handleOrderClick_state_summary: ', state)
    }

    return (
        <ProductCardContainer>
            <ProductCardCard>
                <ProductCardBox>
                    <ProductCardContent>
                        <h3>Business Card</h3>
                        <p>Business Card with the custom properties awailable in our service. You can customise your card with the tons of settings!</p>
                        <div><span onClick={handleOrderClick}>Order Now</span></div>
                    </ProductCardContent>
                </ProductCardBox>
            </ProductCardCard>
        </ProductCardContainer>
    );
};

export default ProductCard;
