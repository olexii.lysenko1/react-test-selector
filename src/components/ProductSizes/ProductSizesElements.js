import styled from 'styled-components';
import { IoIosCheckmark } from 'react-icons/io';

export const ProductSizeWrapper = styled.div`
    position: relative;
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 25px;
    margin-top: 17px;
    color: #232427;

    .c-react-select-sizes {
        width: 400px;

        &__control {
            background: #2a2b2f;
            position: relative;
            top: 20;
            left: 0;
        }

        &__single-value {
            padding-left: 20px;
        }

        &__placeholder {
            padding-left: 20px;
        }

        &__value-container {
            display: flex;
            color: rgba(255,255,255,0.5);

            &:focus {
                border: 9px solid rgba(255,255,255,0.5);
            }
        }

        &__menu-list {
            padding-top: 0px;
            padding-bottom: 0px;
            background: #2a2b2f;
            border-radius: 9px !important;
            border: none;
        }

        &__menu {
            background: #2a2b2f;
            border-radius: 10px !important;
            border: 1px solid #1e1f23;
        }

        &__option {
            background: #2a2b2f;
            padding-top: 5px;
            padding-bottom: 5px;
            padding-left: 0px;
            color: rgba(255,255,255,0.5);

            &:hover {
                background: #1e1f23 !important;
            }
        }
    }
}

`;

export const CheckmarkIcon = styled(IoIosCheckmark)`
    margin-left: 5px;
    margin-top: 10px;
    width: 25px;
    color: rgba(255,255,255,0.5);
`;
