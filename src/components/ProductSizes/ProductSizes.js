import React, { useContext } from 'react';
import Select, { components } from 'react-select';

import { StoreContext } from '../../store/storeProvider'
import { setSize } from '../../store/actions/businessCardsActions'
import { setIsSummaryShown } from '../../store/actions/summaryActions'
import businessCardsService from '../../services/businessCardsService'
import productSizeService, { customStyles } from '../../services/productSizesService'

import {
    ProductSizeWrapper,
    CheckmarkIcon,
} from './ProductSizesElements';

const ProductSizes = ({ data }) => {
    const [state, dispatch] = useContext(StoreContext);
    const sizesOptions = businessCardsService.getNormalizedOptions(data.options);
    const { SingleValue } = productSizeService;

    const handleSizeChange = (data) => {
        const { value } = data;
        dispatch(setSize(value))
        dispatch(setIsSummaryShown(false))
    };

    const Option = (props) => (
        <components.Option {...props} >
            <CheckmarkIcon className={`${state.businessCards.size !== props.data.value ? 'invisible' : ''}`} />
            <label>{props.label}</label>
        </components.Option>
    );

    return <ProductSizeWrapper >
        <Select
            className="c-react-select-sizes"
            classNamePrefix="c-react-select-sizes"
            placeholder={data.title}
            options={sizesOptions}
            components={{ Option, SingleValue }}
            onChange={(value) => handleSizeChange(value)}
            isSearchable={false}
            styles={customStyles}
        />
    </ProductSizeWrapper>
}

export default ProductSizes;
