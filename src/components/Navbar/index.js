import React, { useEffect } from 'react';
import {
    Nav,
    NavLink,
    NavMenu,
    CartIcon,
    NavMarker
} from './NavbarElements';

const Navbar = () => {

    useEffect(() => {
        const marker = document.querySelector('#marker');
        const item = document.querySelectorAll('nav a');

        function Indicator(e) {
            marker.style.left = e.offsetLeft + 'px';
            marker.style.width = e.offsetWidth + 'px';
        }

        item.forEach(link => {
            link.addEventListener('mousemove', (e) => {
                Indicator(e.target);
            })
        })
    }, [])

    return (
        <>
            <Nav>
                <NavMenu>
                    <NavLink to='/' activeStyle>
                        Home
                    </NavLink>
                    <NavLink to='/product/businesscards' activeStyle>
                        Businesscards
                    </NavLink>
                    <NavLink to='/product/posters' activeStyle>
                        Posters
                    </NavLink>
                    <NavLink to='/product/flyers' activeStyle>
                        Flyers
                    </NavLink>
                    <NavLink to='/cart'>
                        <CartIcon />
                    </NavLink>
                    <NavMarker id="marker" />
                </NavMenu>
            </Nav>
        </>
    );
};

export default Navbar;
