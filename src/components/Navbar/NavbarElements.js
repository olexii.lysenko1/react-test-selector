import { FaCartPlus } from 'react-icons/fa';
import { NavLink as Link } from 'react-router-dom';
import styled from 'styled-components';

export const Nav = styled.nav`
    position: relative;
    display: flex;
    justify-content: center;
    align-items: center;
`;

export const NavLink = styled(Link)`
    position: relative;
    color: #fff;
    font-size: 1em;
    text-decoration: none;
    display: inline-block;
    padding: 10px 20px;
    font-weight: 300;
    background: rgba(255,255,255,0.05);
    box-shadow: 0 15px 35px rgba(0,0,0,0.2);
    border-top: 1px solid rgba(255,255,255,0.1);
    border-bottom: 1px solid rgba(255,255,255,0.1);
    z-index: 1000;
    overflow: hidden;
    backdrop-filter: blur(15px);

  cursor: pointer;

  &::before {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 50%;
    background: rgba(255,255,255,0.1);
  }

    &:first-child {
        border-top-left-radius: 30px;
        border-bottom-left-radius: 30px;
    }

    &:nth-last-child(2) {
        border-top-right-radius: 30px;
        border-bottom-right-radius: 30px;
    }

    &:nth-child(2):hover ~ #marker::before {
    background: #ff0;
    box-shadow: 0 0 15px #ff0,
    0 0 30px #ff0,
    0 0 45px #ff0,
    0 0 60px #ff0;
    }

    &:nth-child(3):hover ~ #marker::before {
    background: #0f0;
    box-shadow: 0 0 15px #0f0,
    0 0 30px #0f0,
    0 0 45px #0f0,
    0 0 60px #0f0;
    }

    &:nth-child(4):hover ~ #marker::before {
    background: #ff308f;
    box-shadow: 0 0 15px #ff308f,
    0 0 30px #ff308f,
    0 0 45px #ff308f,
    0 0 60px #ff308f;
    }
`;

export const NavMenu = styled.div`
  display: flex;
  position: relative;
  align-items: center;

  @media screen and (max-width: 768px) {
    display: none;
  }
`;

export const NavMarker = styled.div`
    position: absolute;
    top: 5;
    right: 5%;
    width: 31px;
    height: 31px;
    z-index: 1;
    transition: 0.5s;


    &::before {
        content: '';
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        width: 40px;
        height: 40px;
        border-radius: 50px;
        background: #5da6ff;
        box-shadow: 0 0 15px #5da6ff,
        0 0 30px #5da6ff,
        0 0 45px #5da6ff,
        0 0 60px #5da6ff;
`;

export const CartIcon = styled(FaCartPlus)`
  margin: 0 auto;
    width: 40px;
  color: #fff;
`;
