
import React from 'react';
import {
    ProductPageContainer
} from './ProductPageElements';

const ProductPage = ({ children }) => {
    return (
        <ProductPageContainer>
            {children}
        </ProductPageContainer>
    );
};

export default ProductPage;
