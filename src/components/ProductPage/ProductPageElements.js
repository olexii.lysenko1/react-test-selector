import styled from 'styled-components';

export const ProductPageContainer = styled.div`
  display: flex;
  position: relative;
  align-items: start;
  padding: 40px;
`;
