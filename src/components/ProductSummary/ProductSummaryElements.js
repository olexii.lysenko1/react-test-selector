import styled from 'styled-components';

export const ProductSummaryContainer = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: start;
    flex-wrap: wrap;
    max-width: 1200px;
    margin: -40px 0px 0px 40px;
`;

export const ProductSummaryCard = styled.div`
    position: relative;
    min-width: 320px;
    height: 140px;
    border: 1px solid rgba(0,0,0,0.5);
    border-radius: 5px;
    color: rgba(255,255,255,0.5);
    padding: 15px;
    opacity: 0.3;
`;

export const ProductSummaryItem = styled.p`
    margin: 5px auto;
`;
