import React, { useContext } from 'react';
import { StoreContext } from '../../store/storeProvider'
import {
    ProductSummaryContainer,
    ProductSummaryCard,
    ProductSummaryItem,
} from './ProductSummaryElements';

const ProductSummary = () => {
    const [state, dispatch] = useContext(StoreContext);

    return (
        <ProductSummaryContainer>
            <ProductSummaryCard>
                <h3> Summary: </h3>
                <ProductSummaryItem>BusinessCards: {JSON.stringify(state.businessCards)}</ProductSummaryItem>
                <ProductSummaryItem>Flyers: {JSON.stringify(state.flyers)}</ProductSummaryItem>
                <ProductSummaryItem>Posters: {JSON.stringify(state.posters)}</ProductSummaryItem>
            </ProductSummaryCard>
        </ProductSummaryContainer>
    );
};

export default ProductSummary;
