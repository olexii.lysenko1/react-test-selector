export const PROPERTIES = Object.freeze({
    SIZE: 'size',
});


export default {
    getNormalizedOptions(options) {
        return options.map(option => ({
            value: option.slug,
            label: option.name || `Custom: max_height ${option.customSizes.maxHeight}, max_width ${option.customSizes.maxWidth}`
        }))
    }
}
