import React from 'react';

export const customStyles = {
    option: (provided) => ({
        ...provided,
        backgroundColor: 'grey',
        width: '400px'
    }),
    control: base => ({
        ...base,
        border: 0,
        // NOTE: This line disable the blue border
        boxShadow: 'none'
    })
};

export default {
    SingleValue({ data }) {
        return (
            <div className="c-react-select-sizes__single-value">
                <div>{data.label}</div>
            </div>
        )
    },
}
